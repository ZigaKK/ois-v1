import java.util.Scanner

public class gravitacija
{
    public static void main(String [ ] args)
    {
        Scanner sc = new Scanner(System.in);
		int v = sc.nextInt();
		
		double a = izracun(v);
		izpis(v, a);
    }
    
    static double izracun(int v)
    {
		double c = 6.674 * Math.pow(10, -11);
		double m = 5.972 * Math.pow(10, 24);
		double r = 6.371 * Math.pow(10, 6);
		
		return ((c*m)/Math.pow(r+v, 2));
	}
	void izpis(int v , double a)
	{
	    System.out.println(a + " " + v);   
	}
}